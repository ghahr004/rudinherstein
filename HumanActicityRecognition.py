# First lets import the modules that we are going to use in this
# projct.
from pyspark import SparkContext
import pandas as pd
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression
from kafka import KafkaConsumer
#from sklearn.model_selection import StratfiedKFold
#from sklearn.bse import clone

sc = SparkContext()

# After we downloaded the train set and the test set we load them 
# in our computer.
source1 = "/Users/peymanghahremani/Downloads"
source2 = "/human-activity-recognition-with-smartphones/"
har_train = sc.textFile(source1 + source2 + "train.csv")
har_test = sc.textFile(source1 + source2 + "test.csv")

# The zipWithIndex() command returns the har_train/har_test as
# a list of the from (key,index) where the order is preserved.
# As the keys are all in unicode we use the encode() command to
# convert them into string.

train_indexed = har_train.zipWithIndex().map(lambda (key,index): (key.encode(), index)).collect()
test_indexed = har_test.zipWithIndex().map(lambda (key,index): (key.encode(), index)).collect()

# Clearly the first element of both train_indexed/test_indexed represent
# the features whence we have:


features_in_string = train_indexed[0][0]

# Lets take a look at features_in_string. We need to take each feature 
# from this list and the clues are the commas but the commas are also
# used in some of the features. For instance, in the feature 
# "tBodyAcc-arCoefff()-X,4" comma is used so it is not a separator.
# We need to write a function to split these features using the fact 
# that if the function identifies a comma, if we move one forward and
# one backward we need to reach a quotation mark (") otherwise we will
# have more features such that some of them are not true.


def index_of_commas(string):
    Ind=[]
    t=len(string)
    for i in range(t):
        if string[i]=="," and string[i-1]=='"' and string[i+1]=='"':
            Ind.append(i)
    return Ind 

# Lets extract the target name from features in string. We know that
# target name has to "Activity".

index_of_commas_in_features = index_of_commas(features_in_string)
t = len(index_of_commas_in_features)
target_name = features_in_string[index_of_commas_in_features[t-1]+1:]
features_in_string_wo_target = features_in_string[:index_of_commas_in_features[t-1]]

# Now we have the target_name equal to "Activity". We also defined features_in_string_wo_target
# to be all features except the last one which is stored as the target_name.

# We will use the following function to gather all of features from features_in_string_wo_target.

def list_builder(string):
	indeces = index_of_commas(string)
	r = len(indeces)
	list_of_strings = []
	for i in range(r):
		if i == 0:
			word = string[0:indeces[0]]
			list_of_strings.append(word)
		elif i<r-1:
			a = indeces[i-1] + 1 
			b = indeces[i]
			word = string[a : b] 
			list_of_strings.append(word)
		else:
			a = indeces[r-1] + 1 
			word = string[a:]
			list_of_strings.append(word)
	return list_of_strings
	
features_names = list_builder(features_in_string_wo_target)

# Disregarding the features, the rest of the rows in train_indexed/test_indexed
# are consisted of strings each contains numbers splitted from
# each other with commas. We do not have to find commas in 
# these materials like we did for features.

Train_dataset_with_target = []
Test_dataset_with_target = []

for i in range(len(train_indexed)):
    if i !=0:
        Train_dataset_with_target.append(train_indexed[i][0].split(","))

for i in range(len(test_indexed)):
    if i !=0:
        Test_dataset_with_target.append(test_indexed[i][0].split(","))



# In each of the elements of Train_dataset_with_target/Test_dataset_with_target the last 
# two objects are not numbers. We guess that The first is an stringed integer while the second
# indicates the activity. Let's examine if our guess is right.

The_first_from_end = []
The_second_from_end = []

for i in range(len(Train_dataset_with_target)):
    if Train_dataset_with_target[i][-2: ][1] not in The_first_from_end:
        The_first_from_end.append(Train_dataset_with_target[i][-2: ][1])
    if Train_dataset_with_target[i][-2: ][0] not in The_second_from_end:
        The_second_from_end.append(Train_dataset_with_target[i][-2: ][0])

# print(The_first_from_end) gives six elements '"WALKING"', '"SITTING"', '"LAYING"' and ...
# print(The_second_from_end) gives 1, 3, 5, 6, 7, 8, 11, 14, 15, 16, 17,
# 19, 21, 22, 23, 25, 26, 27, 28, 29, 30.

target_list_train = []
target_list_test = []

for i in range(len(Train_dataset_with_target)):
    e = Train_dataset_with_target[i][-2: ][1] 
    f = The_first_from_end.index(e) + 1 
    target_list_train.append(f)

for i in range(len(Test_dataset_with_target)):
    e = Test_dataset_with_target[i][-2: ][1]
    f = The_first_from_end.index(e) + 1
    target_list_test.append(f)


# The above two set of codes give the target lists for both training
# and testing in numbers. Thus, WALKING is denoted by 1, SITTING is 
# denoted by 2 and ....

# Lets convert both target lists for training and testing into
# arrays. We have:

target_train_data = np.asarray(target_list_train)
target_test_data = np.asarray(target_list_test)

# Done with target for both training and testing. Let's set up the 
# datasets for both training and testing.


train_datalist_string = []
test_datalist_string = []

# In the following two sets of codes we build up the dataset for both
# training and testing by diregarding the target and and the string element
# next to target. ( We know that the second element from left hast
# to be a number between 1 to 30 and it won't have an impact on
# our calculations if we disregard them.)

for i in range(len(Train_dataset_with_target)):
    train_datalist_string.append(Train_dataset_with_target[i][ : -2])

for i in range(len(Test_dataset_with_target)):
    test_datalist_string.append(Test_dataset_with_target[i][ : -2])

# Note that both train_datalist_string and test_datalist_string are
# lists of lists of strings. Let's convert the list of strings into
# lists of floats.

train_data = []
test_data = []

def string_to_float(list_of_stings):
    S = []
    for a in list_of_stings:
        S.append(float(a))
    return S
    
for i in range(len(train_datalist_string)):
    train_data.append(string_to_float(train_datalist_string[i]))

for i in range(len(test_datalist_string)):
    test_data.append(string_to_float(test_datalist_string[i]))

# Let's put our materials in DataFrames.

har_train_data = pd.DataFrame(train_data)
har_test_data  = pd.DataFrame(test_data)  

# We can index the columns of our dataset with feature names we obtained.
# We have:

har_train_data.columns = features_names
har_test_data.columns = features_names


# We can index the columns of our dataset with feature names we obtained.
# We have:

# Now everything is ready for us to start prediciting!

X_train_data = har_train_data
y_train_data = target_train_data
X_test_data = har_test_data
y_test_data = target_test_data



sgd_clf = SGDClassifier(random_state = 42)
sgd_clf.fit(X_train_data,y_train_data)
# print(sgd_clf.predict(X_test)) gives [1 1 1 ..., 6 4 4]

# Lets see how accurate our prediction is:
print("The accuracy is: ")
print(sgd_clf.fit(X_train_data,y_train_data).score(X_test_data,y_test_data))

# The prediction is true for 88 percent.


# Let's train a DecisionTreeRegressor. This is a powerful model, capable of finding
# complex nonlinear relationships in the data. 

tree_reg = DecisionTreeRegressor()
tree_reg.fit(X_train_data,y_train_data)
print(tree_reg.predict(X_train_data)) 
tree_mse_train_data = mean_squared_error(y_train_data, tree_reg.predict(X_train_data))
tree_rmse_train_data = np.sqrt(tree_mse_train_data)

# print(tree_rmse_train) gives 0.0!. Though it looks a perfect model, DecisionTreeRegressor
# usually does not do well when it boils down to the test set. 

tree_mse_test_data = mean_squared_error(y_test_data, tree_reg.predict(X_test_data))
tree_rmse_test_data = np.sqrt(tree_mse_test_data)


# print(tree_rmse) gives: 0.472163723308. It is not even 50 percent accuracy. 


# Let's use the cross_val_score() function to evaluate SGDClassifier model using K-fold 
# cross-validation. Let's recall that K-fold cross-validation means splitting the training set 
# into K-folds, then making predictions and evaluating them on each fold using a model 
# obtained on the remaining fold.

print(cross_val_predict(sgd_clf, X_train_data, y_train_data, cv =3)) #gives: 
# [1 1 1 ..., 4 4 4]
print(cross_val_score(sgd_clf, X_train_data, y_train_data, cv=3, scoring="accuracy")) #gives: 
# [ 0.92047308  0.90986949  0.78349673]

softmax_reg = LogisticRegression(multi_class="multinomial", solver="lbfgs", C=10)
softmax_reg.fit(X_train_data,y_train_data)
print("The accuracy of our model with Softmax Regreesion is: ")
print(softmax_reg.fit(X_train_data,y_train_data).score(X_test_data,y_test_data))