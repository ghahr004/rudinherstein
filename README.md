Human Activity Recognition with Smartphones
In this project, I used the data given on
https://www.kaggle.com/uciml/human-activity-recognition-with-smartphones
I Used Stochastic Gradient Descent, Decision Tree Regressor, K-fold Cross
Validation and Softmax Regression to predict human activities. The Softmax 
Regression worked best, with 96% accuracy.